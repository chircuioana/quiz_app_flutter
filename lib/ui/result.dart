import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int _score;
  final Function _onTryAgainPressed;

  Result(this._score, this._onTryAgainPressed);

  String _getResult() {
    if (_score < 30) {
      return "😎";
    } else if (_score > 30 && _score < 50) {
      return "😋";
    } else {
      return "🧐";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          _getResult(),
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
          textAlign: TextAlign.center,
        ),
        FlatButton(
          child: Text(
            "Try Again",
            style: TextStyle(color: Colors.deepOrange),
          ),
          onPressed: _onTryAgainPressed,
        )
      ],
    ));
  }
}
