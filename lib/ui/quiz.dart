import 'package:flutter/material.dart';
import 'package:quizappflutter/data/models/question.dart';

class Quiz extends StatelessWidget {
  final Question _currentQuestion;
  final Function _onAnswerClicked;

  Quiz(this._currentQuestion, this._onAnswerClicked);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            _currentQuestion.question,
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          ..._currentQuestion.answers.map((answer) {
            return RaisedButton(
              child: Text(answer.text),
              onPressed: () => _onAnswerClicked(answer.score),
            );
          }).toList(),
        ],
      ),
    );
  }
}
