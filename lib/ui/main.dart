import 'package:flutter/material.dart';
import 'package:quizappflutter/data/models/question.dart';
import 'package:quizappflutter/data/store/questions_store.dart';

import 'quiz.dart';
import 'result.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Quiz',
      theme: ThemeData(
          backgroundColor: Colors.black45,
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.deepOrange),
      home: QuizScreen(title: 'Answer these questions'),
    );
  }
}

class QuizScreen extends StatefulWidget {
  QuizScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _QuizScreenState createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  static QuestionsStore _store = QuestionsStore();
  Question _currentQuestion = _store.getCurrentQuestion();
  var _hasFinished = false;

  void _answerQuestion(int score) {
    setState(() {
      _store.addScore(score);
      if (_store.hasNextQuestion() == false) {
        _hasFinished = true;
        return;
      }
      _hasFinished = false;
      _store.incrementQuestion();
      _currentQuestion = _store.getCurrentQuestion();
    });
  }

  void _restart() {
    setState(() {
      _store.restart();
      _hasFinished = false;
      _currentQuestion = _store.getCurrentQuestion();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: _hasFinished
            ? Result(_store.getScore(), _restart)
            : Quiz(_currentQuestion, _answerQuestion));
  }
}
