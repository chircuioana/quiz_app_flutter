import 'package:quizappflutter/data/models/answer.dart';
import 'package:quizappflutter/data/models/question.dart';

class QuestionsStore {
  static const _questions = const [
    Question(question: "What is your favorite food?", answers: [
      Answer(text: "Burger 🍔", score: 1),
      Answer(text: "Spagetti 🍝", score: 10),
      Answer(text: "Soup 🥘", score: 20),
      Answer(text: "Sushi 🍣", score: 25)
    ]),
    Question(question: "What do you prefer to drink?", answers: [
      Answer(text: "Coffee ☕️", score: 10),
      Answer(text: "Tea 🍵", score: 15),
      Answer(text: "Soda 🥤", score: 5)
    ]),
    Question(question: "What kind of sweets do you prefer?", answers: [
      Answer(text: "Ice cream 🍧", score: 10),
      Answer(text: "Donuts 🍩", score: 5),
      Answer(text: "Cake 🍰", score: 10),
      Answer(text: "Candy 🍬", score: 5)
    ]),
  ];
  var _currentQuestionIndex = 0;
  var _score = 0;

  void incrementQuestion() {
    _currentQuestionIndex++;
  }

  bool hasNextQuestion() {
    return _currentQuestionIndex < _questions.length - 1;
  }

  Question getCurrentQuestion() {
    return _questions[_currentQuestionIndex];
  }

  void addScore(int score) {
    this._score += score;
  }

  int getScore() => _score;

  void restart() {
    _currentQuestionIndex = 0;
    _score = 0;
  }
}
