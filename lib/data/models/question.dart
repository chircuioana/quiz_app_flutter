import 'answer.dart';

class Question {
  final String question;
  final List<Answer> answers;

  const Question({this.question, this.answers});
}
